--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Agen; Type: TABLE; Schema: public; Owner: nabila
--

CREATE TABLE public."Agen" (
    agent_id text[] NOT NULL,
    username text NOT NULL,
    password text NOT NULL
);


ALTER TABLE public."Agen" OWNER TO nabila;

--
-- Name: Category; Type: TABLE; Schema: public; Owner: nabila
--

CREATE TABLE public."Category" (
    category_id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public."Category" OWNER TO nabila;

--
-- Name: Courier; Type: TABLE; Schema: public; Owner: nabila
--

CREATE TABLE public."Courier" (
    courier_id integer NOT NULL,
    courier_name text NOT NULL,
    phone bigint NOT NULL,
    merchant_id integer NOT NULL
);


ALTER TABLE public."Courier" OWNER TO nabila;

--
-- Name: Customer; Type: TABLE; Schema: public; Owner: nabila
--

CREATE TABLE public."Customer" (
    customer_id text NOT NULL,
    name text NOT NULL,
    username text NOT NULL,
    password text[] NOT NULL,
    email text,
    phone integer,
    balance bigint NOT NULL
);


ALTER TABLE public."Customer" OWNER TO nabila;

--
-- Name: Customer_agen; Type: TABLE; Schema: public; Owner: nabila
--

CREATE TABLE public."Customer_agen" (
    customer_id text[] NOT NULL,
    agent_id text[] NOT NULL,
    balance_in integer
);


ALTER TABLE public."Customer_agen" OWNER TO nabila;

--
-- Name: Feedback; Type: TABLE; Schema: public; Owner: nabila
--

CREATE TABLE public."Feedback" (
    feedback_id integer NOT NULL,
    feedback_note text NOT NULL
);


ALTER TABLE public."Feedback" OWNER TO nabila;

--
-- Name: Item; Type: TABLE; Schema: public; Owner: nabila
--

CREATE TABLE public."Item" (
    item_id integer NOT NULL,
    name text NOT NULL,
    price bigint NOT NULL,
    description text NOT NULL,
    photo bit(1),
    merchant_id integer NOT NULL
);


ALTER TABLE public."Item" OWNER TO nabila;

--
-- Name: Merchant; Type: TABLE; Schema: public; Owner: nabila
--

CREATE TABLE public."Merchant" (
    merchant_id integer NOT NULL,
    merchant_name text NOT NULL,
    username text NOT NULL,
    password text NOT NULL
);


ALTER TABLE public."Merchant" OWNER TO nabila;

--
-- Name: PO_item; Type: TABLE; Schema: public; Owner: nabila
--

CREATE TABLE public."PO_item" (
    order_id integer NOT NULL,
    item_id integer NOT NULL,
    quantity integer
);


ALTER TABLE public."PO_item" OWNER TO nabila;

--
-- Name: Purchase_order; Type: TABLE; Schema: public; Owner: nabila
--

CREATE TABLE public."Purchase_order" (
    order_id integer NOT NULL,
    payment_date date NOT NULL,
    payment_type text NOT NULL,
    cart_id integer NOT NULL,
    total_payment bigint NOT NULL,
    address text,
    customer_id text NOT NULL,
    merchant_id text NOT NULL
);


ALTER TABLE public."Purchase_order" OWNER TO nabila;

--
-- Name: Report; Type: TABLE; Schema: public; Owner: nabila
--

CREATE TABLE public."Report" (
    report_id integer NOT NULL,
    date date NOT NULL,
    total_sales integer NOT NULL,
    total_customer integer NOT NULL,
    total_order integer NOT NULL
);


ALTER TABLE public."Report" OWNER TO nabila;

--
-- Name: Shipping; Type: TABLE; Schema: public; Owner: nabila
--

CREATE TABLE public."Shipping" (
    shipping_id integer NOT NULL,
    status text NOT NULL,
    note text,
    date date NOT NULL,
    address text,
    order_id integer NOT NULL,
    courier_id integer NOT NULL
);


ALTER TABLE public."Shipping" OWNER TO nabila;

--
-- Name: Transaction_History; Type: TABLE; Schema: public; Owner: nabila
--

CREATE TABLE public."Transaction_History" (
    transaction_id integer NOT NULL,
    payment_date date NOT NULL,
    payment_type text NOT NULL,
    cart_id integer NOT NULL,
    total_payment bigint NOT NULL,
    address text,
    customer_id text NOT NULL,
    merchant_id integer NOT NULL
);


ALTER TABLE public."Transaction_History" OWNER TO nabila;

--
-- Data for Name: Agen; Type: TABLE DATA; Schema: public; Owner: nabila
--

COPY public."Agen" (agent_id, username, password) FROM stdin;
\.


--
-- Data for Name: Category; Type: TABLE DATA; Schema: public; Owner: nabila
--

COPY public."Category" (category_id, name) FROM stdin;
\.


--
-- Data for Name: Courier; Type: TABLE DATA; Schema: public; Owner: nabila
--

COPY public."Courier" (courier_id, courier_name, phone, merchant_id) FROM stdin;
\.


--
-- Data for Name: Customer; Type: TABLE DATA; Schema: public; Owner: nabila
--

COPY public."Customer" (customer_id, name, username, password, email, phone, balance) FROM stdin;
\.


--
-- Data for Name: Customer_agen; Type: TABLE DATA; Schema: public; Owner: nabila
--

COPY public."Customer_agen" (customer_id, agent_id, balance_in) FROM stdin;
\.


--
-- Data for Name: Feedback; Type: TABLE DATA; Schema: public; Owner: nabila
--

COPY public."Feedback" (feedback_id, feedback_note) FROM stdin;
\.


--
-- Data for Name: Item; Type: TABLE DATA; Schema: public; Owner: nabila
--

COPY public."Item" (item_id, name, price, description, photo, merchant_id) FROM stdin;
\.


--
-- Data for Name: Merchant; Type: TABLE DATA; Schema: public; Owner: nabila
--

COPY public."Merchant" (merchant_id, merchant_name, username, password) FROM stdin;
\.


--
-- Data for Name: PO_item; Type: TABLE DATA; Schema: public; Owner: nabila
--

COPY public."PO_item" (order_id, item_id, quantity) FROM stdin;
\.


--
-- Data for Name: Purchase_order; Type: TABLE DATA; Schema: public; Owner: nabila
--

COPY public."Purchase_order" (order_id, payment_date, payment_type, cart_id, total_payment, address, customer_id, merchant_id) FROM stdin;
\.


--
-- Data for Name: Report; Type: TABLE DATA; Schema: public; Owner: nabila
--

COPY public."Report" (report_id, date, total_sales, total_customer, total_order) FROM stdin;
\.


--
-- Data for Name: Shipping; Type: TABLE DATA; Schema: public; Owner: nabila
--

COPY public."Shipping" (shipping_id, status, note, date, address, order_id, courier_id) FROM stdin;
\.


--
-- Data for Name: Transaction_History; Type: TABLE DATA; Schema: public; Owner: nabila
--

COPY public."Transaction_History" (transaction_id, payment_date, payment_type, cart_id, total_payment, address, customer_id, merchant_id) FROM stdin;
\.


--
-- Name: Category Category_pkey; Type: CONSTRAINT; Schema: public; Owner: nabila
--

ALTER TABLE ONLY public."Category"
    ADD CONSTRAINT "Category_pkey" PRIMARY KEY (category_id);


--
-- Name: Courier Courier_pkey; Type: CONSTRAINT; Schema: public; Owner: nabila
--

ALTER TABLE ONLY public."Courier"
    ADD CONSTRAINT "Courier_pkey" PRIMARY KEY (courier_id);


--
-- Name: Feedback Feedback_pkey; Type: CONSTRAINT; Schema: public; Owner: nabila
--

ALTER TABLE ONLY public."Feedback"
    ADD CONSTRAINT "Feedback_pkey" PRIMARY KEY (feedback_id);


--
-- Name: Merchant Merchant_pkey; Type: CONSTRAINT; Schema: public; Owner: nabila
--

ALTER TABLE ONLY public."Merchant"
    ADD CONSTRAINT "Merchant_pkey" PRIMARY KEY (merchant_id);


--
-- Name: Report Report_pkey; Type: CONSTRAINT; Schema: public; Owner: nabila
--

ALTER TABLE ONLY public."Report"
    ADD CONSTRAINT "Report_pkey" PRIMARY KEY (report_id);


--
-- Name: Transaction_History Transaction_History_pkey; Type: CONSTRAINT; Schema: public; Owner: nabila
--

ALTER TABLE ONLY public."Transaction_History"
    ADD CONSTRAINT "Transaction_History_pkey" PRIMARY KEY (transaction_id);


--
-- Name: Agen agent_id; Type: CONSTRAINT; Schema: public; Owner: nabila
--

ALTER TABLE ONLY public."Agen"
    ADD CONSTRAINT agent_id PRIMARY KEY (agent_id);


--
-- Name: Customer customer_id; Type: CONSTRAINT; Schema: public; Owner: nabila
--

ALTER TABLE ONLY public."Customer"
    ADD CONSTRAINT customer_id PRIMARY KEY (customer_id);


--
-- Name: Customer_agen id; Type: CONSTRAINT; Schema: public; Owner: nabila
--

ALTER TABLE ONLY public."Customer_agen"
    ADD CONSTRAINT id PRIMARY KEY (customer_id, agent_id);


--
-- Name: Item item_id; Type: CONSTRAINT; Schema: public; Owner: nabila
--

ALTER TABLE ONLY public."Item"
    ADD CONSTRAINT item_id PRIMARY KEY (item_id);


--
-- Name: Purchase_order order_id; Type: CONSTRAINT; Schema: public; Owner: nabila
--

ALTER TABLE ONLY public."Purchase_order"
    ADD CONSTRAINT order_id PRIMARY KEY (order_id);


--
-- Name: PO_item purchase_id; Type: CONSTRAINT; Schema: public; Owner: nabila
--

ALTER TABLE ONLY public."PO_item"
    ADD CONSTRAINT purchase_id PRIMARY KEY (order_id, item_id);


--
-- Name: Shipping shipping_id; Type: CONSTRAINT; Schema: public; Owner: nabila
--

ALTER TABLE ONLY public."Shipping"
    ADD CONSTRAINT shipping_id PRIMARY KEY (shipping_id);


--
-- PostgreSQL database dump complete
--

